package main_test

import (
	"encoding/base64"
	"math/rand"
	"strconv"
	"testing"
	"time"
)

func Test212274(t *testing.T) {
	const N = 1_000

	rand.Seed(time.Now().UnixNano())
	for i := 0; i < N; i++ {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			if rand.Float32() < 0.99 {
				t.Log("Success!")
				return
			}
			t.Fail()

			var b [1 << 16]byte
			rand.Read(b[:])
			s := base64.StdEncoding.EncodeToString(b[:])

			for len(s) > 40 {
				t.Log(s[:40] + "\n")
				s = s[40:]
			}
		})
	}
}
